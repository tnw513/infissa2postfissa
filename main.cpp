#include <iostream>
#include "pila.h"
#include "coda.h"
#include "servizio.h"

using namespace std;

int main()
{
    /*
    Pila<char> pila;
    Coda<char> coda;
    cout << "Pila: ";
    insPi(pila);
    stampaPila(pila);
    cout << endl << "Coda: ";
    insCod(coda);
    stampaCoda(coda);
    cout<<endl;
*/
//string a= "598+46**7+*";
    Coda<char> postfcoda;
    //string a="(5*(((9+8)*(4*6))+7))";
    //string b="(3+(1*2)/(5-1))";
    string a;
    cout<<"Inserire stringa da convertire e valutare: ";
    cin>>a;
    string postf=infissa2postfissa(a,postfcoda);
    int val=valutaPostfissa(postf);
    cout<<endl<<"Infissa: " <<a<<endl<<"Postfissa: "<<postf<<endl<<"Risultato: "<<val<<endl;
    cout<<"Postfissa da coda: ";
    stampaCoda(postfcoda);
    cout<<endl;
    return 0;
}
