template <class T>
CellaPC<T>::CellaPC()
{
    //ctor
}

template <class T>
CellaPC<T>::~CellaPC()
{
    //dtor
}

template <class T>
CellaPC<T>::CellaPC(const tipoelem &e) {

    elemento=e;
}

template <class T>
void CellaPC<T>::setElem(tipoelem e) {

    elemento=e;
}

template <class T>
typename CellaPC<T>::tipoelem CellaPC<T>::getElem() const{

    return elemento;
}



template <class T>
void CellaPC<T>::setPrec(CellaPC<T> *p) {

    prec=p;
}

template <class T>
CellaPC<T> * CellaPC<T>::getPrec() const{

    return prec;
}



template <class T>
void CellaPC<T>::setSucc(CellaPC<T> *p) {

    succ=p;
}

template <class T>
CellaPC<T> * CellaPC<T>::getSucc() const{

    return succ;
}

template <class T>
bool CellaPC<T>::operator==(CellaPC<T> c) {

    return (getElem()==c.getElem());
}
