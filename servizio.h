#ifndef SERVIZIO_H_INCLUDED
#define SERVIZIO_H_INCLUDED

#include <string>
#include <iostream>
#include "pila.h"
#include "coda.h"

using namespace std;

template <class T>
void stampaPila(Pila<T> & P)
{

    while(!P.pilaVuota())
    {

        cout<<P.leggiPila()<<" ";
        P.fuoriPila();
    }
}

template <class T>
void insPi(Pila<T> & P)
{
    char lettera='a';
    for(int i=0; i<20; i++,lettera++)
        P.inPila(lettera);

}

template <class T>
void insCod(Coda<T> &C)
{
    char lettera='a';
    for (int i=0; i<20; i++,lettera++)
        C.inCoda(lettera);

}

template <class T>
void stampaCoda(Coda<T> &c)
{

    while (!c.codaVuota())
    {

        cout<<c.leggiCoda()<<" ";
        c.fuoriCoda();
    }
}

int valutaPostfissa(string a)
{
    int N = a.size();
    Pila<int> postf;
    int el;
    for (int i = 0; i < N; i++)
    {
        if (a[i] == '+')
        {
            el = postf.leggiPila();
            postf.fuoriPila();
            int ris = el + postf.leggiPila();
            postf.fuoriPila();
            postf.inPila(ris);

        }
        else if (a[i] == '*')
        {
            el = postf.leggiPila();
            postf.fuoriPila();
            int ris = el * postf.leggiPila();
            postf.fuoriPila();
            postf.inPila(ris);
        }
        else if (a[i] == '-')
        {
            el = postf.leggiPila();
            postf.fuoriPila();
            int ris = postf.leggiPila() - el;
            postf.fuoriPila();
            postf.inPila(ris);

        }
        else if (a[i] == '/')
        {
            el = postf.leggiPila();
            postf.fuoriPila();
            int ris = postf.leggiPila() / el;
            postf.fuoriPila();
            postf.inPila(ris);
        }
        else if ((a[i] >= '0') && (a[i] <= '9')) postf.inPila(a[i]-'0');
        else return -255; // valore provvisorio di errore
        /*
        while ((a[i] >= '0') && (a[i] <= '9'))
        {
            el = postf.leggiPila();
            postf.fuoriPila();
            postf.inPila(10 * el + (a[i++]-'0'));

        }
        */
        // cout << postf.leggiPila() << endl;
    }
    //cout << endl << postf.leggiPila() << endl;
    int val=postf.leggiPila();
    return val;
}

string infissa2postfissa(string a,Coda<char> &postfcoda)
{
    string postf="";
    int N = a.size();
    Pila<char> operatori;
    for (int i = 0; a[i]!='\0'; i++)
    {
        //if (i>0)
        //    cout<<endl<<operatori.leggiPila();

        if (a[i] == '(')
            operatori.inPila(a[i]);

        if (a[i] == ')')
        {
            while (operatori.leggiPila()!='(')
            {
                //cout << operatori.leggiPila() << " ";
                postfcoda.inCoda(operatori.leggiPila());
                postf=postf+operatori.leggiPila(); // per ritorno stringa
                operatori.fuoriPila(); // si esegue anche in caso di parentesi

            }
            operatori.fuoriPila(); // si esegue anche in caso di parentesi

        }

        if ((a[i] == '+') || (a[i] == '-'))
        {
            if(operatori.leggiPila()!='(')
            {
                postfcoda.inCoda(a[i]);
                postf=postf+a[i];
            }
            else
                operatori.inPila(a[i]);

        }

        if ((a[i] == '*') || (a[i] == '/'))
        {
            if(operatori.leggiPila()=='+' || operatori.leggiPila()=='-' || operatori.leggiPila()=='(')
                operatori.inPila(a[i]);
            else if(operatori.leggiPila()=='*' || operatori.leggiPila()=='/')
            {
                postfcoda.inCoda(a[i]);
                postf=postf+a[i];
            }

        }
        if (((a[i] >= '0') && (a[i] <= '9')) || ((a[i] >= 'a') && (a[i] <= 'z')))
        {
            //cout << a[i] << " ";
            postfcoda.inCoda(a[i]);
            postf=postf+a[i]; // per ritorno stringa
        }

    }
    cout << endl;
    return postf;
}

#endif // SERVIZIO_H_INCLUDED
