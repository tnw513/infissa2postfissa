#include <assert.h>

template <class T>
Coda<T>::Coda()
{
creaCoda();
}

template <class T>
Coda<T>::~Coda()
{
    while(lunghezza>0)
        fuoriCoda();
}

template <class T>
void Coda<T>::creaCoda() {

//    typename CellaPC<T>::tipoelem nullo; //chi lo inizializza?
    coda=new CellaPC<T>; // controllare. Unico metodo per non far crashare
    testa=coda;
    lunghezza=0;
//    coda->setElem(nullo);
//    coda->setSucc(coda);
}

template <class T>
bool Coda<T>::codaVuota() const {

    return lunghezza==0;
}
/*
template <class T>
typename Coda<T>::posizione Coda<T>::testaCoda() const{
    return (testa->getSucc());
}
*/
template <class T>
void Coda<T>::inCoda(typename CellaPC<T>::tipoelem e) {

    coda->setElem(e);
    coda->setSucc(new CellaPC<T>);
    coda=coda->getSucc();
    lunghezza++;
}

template <class T>
void Coda<T>::fuoriCoda() {
    posizione temp;

    if (!codaVuota()) {
        temp=testa;
        testa=testa->getSucc();
        delete(temp);
        lunghezza--;
    }
}

template <class T>
typename CellaPC<T>::tipoelem Coda<T>::leggiCoda() const{

    assert (!codaVuota());
        return testa->getElem(); //da rivedere

}
