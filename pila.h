#ifndef PILA_H
#define PILA_H

#include "cella.h"
#include <iostream>
using namespace std;

template <class T>
class Pila
{
    public:
        typedef CellaPC<T> *posizione;
        //typedef T tipoelem;
        Pila();
        virtual ~Pila();
        void creaPila();
        bool pilaVuota() const;
        posizione testaPila() const;
        void inPila(typename CellaPC<T>::tipoelem);
        void fuoriPila();
        typename CellaPC<T>::tipoelem leggiPila() const;
    private:
        //posizione coda; // ex elementi
        posizione testa;
        int lunghezza;
};

#include "pila.tcc"
#endif // PILA_H
