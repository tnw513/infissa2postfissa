#ifndef CODA_H
#define CODA_H

#include "cella.h"
#include <iostream>
using namespace std;

template <class T>
class Coda
{
    public:
        typedef CellaPC<T> *posizione;
        //typedef T tipoelem;
        Coda();
        virtual ~Coda();
        void creaCoda();
        bool codaVuota() const;
        posizione testaCoda() const;
        void inCoda(typename CellaPC<T>::tipoelem);
        void fuoriCoda();
        typename CellaPC<T>::tipoelem leggiCoda() const;
    private:
        posizione coda; // ex elementi
        posizione testa;
        int lunghezza;
};

#include "coda.tcc"

#endif // CODA_H
