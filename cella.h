#ifndef CELLAPC_H
#define CELLAPC_H

template <class T>
class CellaPC
{
    public:
        typedef T tipoelem;
        CellaPC();
        virtual ~CellaPC();
        CellaPC(const tipoelem &);
        void setElem(tipoelem);
        tipoelem getElem() const;
        void setPrec(CellaPC *);
        CellaPC * getPrec() const;
        void setSucc(CellaPC *);
        CellaPC * getSucc() const;
        bool operator ==(CellaPC);
    private:
        tipoelem elemento;
        CellaPC *succ;
        CellaPC *prec;
};

#include "cella.tcc"

#endif // CELLAPC_H
