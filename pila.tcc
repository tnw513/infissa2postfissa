#include <assert.h>

template <class T>
Pila<T>::Pila()
{
creaPila();
}

template <class T>
Pila<T>::~Pila()
{
    while(lunghezza>0)
        fuoriPila();
    delete(testa); //sentinella
}

template <class T>
void Pila<T>::creaPila() {

//    typename CellaPC<T>::tipoelem nullo; //chi lo inizializza?
    testa=new CellaPC<T>; // controllare. Unico metodo per non far crashare
    //testa=testa;
    lunghezza=0;
//    testa->setElem(nullo);
//    testa->setSucc(testa);
}

template <class T>
bool Pila<T>::pilaVuota() const {

    return lunghezza==0;
}
/*
template <class T>
typename Pila<T>::posizione Pila<T>::testaPila() const{
    return (testa->getSucc());
}
*/
template <class T>
void Pila<T>::inPila(typename CellaPC<T>::tipoelem e) {

    testa->setSucc(new CellaPC<T>);
    testa->getSucc()->setPrec(testa);
    testa=testa->getSucc();
    testa->setElem(e);
    lunghezza++;
}

template <class T>
void Pila<T>::fuoriPila() {
    posizione temp;

    if (!pilaVuota()) {
        temp=testa;
        testa=testa->getPrec();
        delete(temp);
        lunghezza--;
    }
}

template <class T>
typename CellaPC<T>::tipoelem Pila<T>::leggiPila() const{

    assert (!pilaVuota());
        return testa->getElem(); //da rivedere

}

